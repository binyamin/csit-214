#include <iostream>

using namespace std;

const int row_count = 7;
const int col_count = 4;
char chart[row_count][col_count];

const int seats_total = row_count * col_count;
int seats_taken = 0;

void assign_seat(int row, int col) {
    if(chart[row][col] == 'X') {
        cerr << "Seat taken! Try again." << endl;
    } else {
        chart[row][col] = 'X';
        seats_taken++;
        cout << "Choice confirmed." << endl;
    }
}


void printSeats() {
    for (int i = 0; i < row_count; i++) {
        cout << i + 1;

        for (auto &&j : chart[i]) {
            cout << " " << j;
        }
    
        cout << endl;
    }
}

int main(int argc, char const *argv[])
{
    for (auto i: chart) {
        i[0] = 'A';
        i[1] = 'B';
        i[2] = 'C';
        i[3] = 'D';
    }

    do {
        cout << endl;
        printSeats();
        cout << "Enter your seat choice (1a - 7d, ctrl+c to exit): ";

        int row;
        char col;
        cin >> row >> col;

        row--; // if user says "1A", the array index is "0,0";
        switch (col) {
            case 'A':
            case 'a':
                assign_seat(row, 0);
                break;
            case 'B':
            case 'b':
                assign_seat(row, 1);
                break;
            case 'C':
            case 'c':
                assign_seat(row, 2);
                break;
            case 'D':
            case 'd':
                assign_seat(row, 3);
                break;
            default:
                cerr << "Not an option. Try again." << endl;
                break;
        }
    } while(seats_taken != seats_total);

    cout << "All seats are taken. Goodbye." << endl;
    return 0;
}
