#include <iostream>
#include <iomanip>

using namespace std;

/*
    ---Chapter 2---
    Author: Binyamin Green
    Class:  CSIT 214
    Date:   9/12/2021
*/

void compute_loan(
    int net_gain,
    float rate, /* yearly interest, as a decimal */
    int duration /* in months */
) {
    float years = (float (duration) / 12);

    // This algorithm gets the value of a loan, based on the return value
    float initial_value = net_gain / ( 1 - ( rate * years ) );

    // This algorithm gets the monthly installment for the above loan
    float monthly_payment = initial_value / duration;
    
    cout << "Monthly payment: $";
    cout << fixed << setprecision(2) << monthly_payment;
    cout << endl;
    
    cout << "Initial input: $";
    cout << fixed << setprecision(2) << initial_value;
    cout << endl;
}

int main(int argc, char* argv[]) {
    compute_loan(775, 0.15, 18);
    return 0;
}
