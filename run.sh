#!/usr/bin/env bash

# Suppress output:
# $command_string >/dev/null 2>&1

latest_dir=`ls | grep "chapter-[[:digit:]]" | sort -n -r | head -n1`
builddir="build"

ansi_yellow() {
    printf "\033[38;5;3m$@\033[39m"
}

ansi_bold() {
    printf "\033[1m$@\033[22m"
}

error() {
    # Print an error message (write to stderr)
    echo -e "`ansi_yellow Error`: $@" 1>&2
}

# Is 1st param present?
# if [[ ! -n $1 ]]; then
#     error "First argument should be a folder name (a relative path)"
#     exit 1
# fi

# Does 1st param point to a real directory?
# if [[ ! -d $(pwd)/$1 ]]; then
#     error "The folder you gave doesn't seem to exist"
#     exit 127
# fi

(
    cd $latest_dir
    
    echo `ansi_bold "Building [meson]"`
    meson install -C $builddir > /dev/null || exit 1

    echo `ansi_bold "Running [exe]"`
    ./build/app ${@:1}
)