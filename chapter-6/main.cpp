#include <fstream>
#include <iostream>

using namespace std;

int main(int argc, char const *argv[]) {
    ifstream infile("input.txt");
    ofstream outfile("output.txt");

    if (infile.fail()) {
        cerr << "Error opening a file" << endl;
        infile.close();
        exit(1);
    }

    string firstname, lastname;
    int a,b,c,d,e,f,g,h,i,j;
    while(infile >> lastname >> firstname >> a >> b >> c >> d >> e >> f >> g >> h >> i >> j) {
        double avg = (a + b + c + d + e + f + g + h + i + j) / 10;
        outfile << lastname << " " << firstname << " ";
        outfile << a << " " << b << " " << c << " " << d << " " << e << " ";
        outfile << f << " " << g << " " << h << " " << i << " " << j << " ";
        outfile << avg << endl;
    }
    
    infile.close();
    outfile.close();
    return 0;
}