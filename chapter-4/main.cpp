#include <iostream>

using namespace std;

int toMinutes(int h, int m, bool isAM) {
    int returnValue = m;
    
    // Remember that 12:01am is really 0h, 1m (not 12h, 1m)
    if ((h % 12) > 0) {
        returnValue += (h * 60);
    }
    
    // If it's PM, add 12h from AM
    if(not isAM) {
        returnValue += (12 * 60);
    }

    return returnValue;   
}

// Returns difference in minutes
int computeDifference(int h1, int m1, bool isAM1, int h2, int m2, bool isAM2) {
    int t1 = toMinutes(h1, m1, isAM1);
    int t2 = toMinutes(h2, m2, isAM2);

    // if t2 is earlier 
    if(t2 < t1) {
        t2 += (24 * 60);
    }

    return t2 - t1;
}

void test(string msg, int h1, int m1, bool isAM1, int h2, int m2, bool isAM2) {
    cerr << msg << "\t=>";
    int r = computeDifference(h1, m1, isAM1, h2, m2, isAM2);
    cerr << r / 60 << ":" << r %60 << endl;
}

int main(int argc, char const *argv[])
{
    cerr << "Running test suite..." << endl << endl;
    // :: Same day
    cerr << ":: Same Day" << endl;
    test("1pm -> 2pm (1hr)", 1,0,false, 2,0, false);
    test("10am -> 11am (1hr)", 10,0,true, 11,0, true);
    test("10am -> 2pm (4hr)", 10,0,true, 2,0,false);
    test("10am -> 2:30pm (4.5hr)", 10,0,true, 2,30,false);
    cerr << endl;

    // :: Other day
    cerr << ":: Other Day" << endl;
    test("10pm -> 1am (3hr)", 10,0, false, 1, 0, true);
    test("2pm -> 1pm (23hr)", 2,0, false, 1, 0, false);
    test("11:30pm -> 2am (2.5hr)", 11,30, false, 2, 0, true);
    cerr << endl;


    // :: Other AM/PM
    cerr <<  ":: Other AM/PM" << endl;
    test("10am - 12am (14hr)", 10, 0, true, 12, 0, true);
    test("10am - 12:30am (14.5hr)", 10, 0, true, 12, 30, true);

    return 0;
}