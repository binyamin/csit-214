# Chapter 4
## Project
Your time machine is capable of going forward in time up to 24 hours. The machine is configured to jump ahead in minutes. To enter the proper number of minutes into your machine, you would like a program that can take a start time (in hours, minutes, and a Boolean indicating AM or PM) and a future time (in hours, minutes, and a Boolean indicating AM or PM) and calculate the difference in minutes between the start and future time.

A time is specified in your program with three variables:
```cpp
int hours, minutes;
bool isAM;
```

For example, to represent 11:50 PM, you would store:
```cpp
hours = 11
minutes = 50
isAM = false;
```
This means that you need six variables to store a start and future time.

Write a program that allows the user to enter a start time and a future time. Include a function named `computeDifference` that takes the six variables as parameters that represent the start time and future time. Your function should return, as an `int`, the time difference in minutes. For example, given a start time of 11:59 AM and a future time of 12:01 PM, your program should compute 2 minutes as the time difference. Given a start time of 11:59 AM and a future time of 11:58 AM, your program should compute 1439 minutes as the time difference (23 hours and 59 minutes).

You may need “AM” or “PM” from the user’s input by reading in two character values. (_Display 2.3_ illustrates character input.) Characters can be compared just like numbers. For example, if the variable `aChar` is of type `char`, then (`aChar == 'A'`) is a Boolean expression that evaluates to `true` if `aChar` contains the letter A.