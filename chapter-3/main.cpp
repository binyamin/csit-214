#include <iostream>
#include <string.h>

using namespace std;

bool is_weekend(char const *str) {
    return (
        not (bool) strcasecmp((char const *) "Su", str)
        or
        not (bool) strcasecmp((char const *) "Sa", str)
    );
}

bool is_daytime(string str) {
    int hours = stoi(str.substr(0, str.find_first_of(":")));
    int min = stoi(str.substr(str.find_first_of(":") + 1));
    
    // Convert to decimal, to handle minutes and hours at once
    float hours_decimal = (float) hours + ( (float) min / 60);

    return (hours_decimal >= 8.0 && hours_decimal <= 18.00);
}

float compute_cost(char const *day_of_week, char const *time, int length_of_call)
{
    float rate;
    if(is_weekend(day_of_week)) {
        // It's "su" or "sa"
        rate = 0.15;
    } else {
        // It's M-F
        if (is_daytime(time)) {
            rate = 0.40;
        } else {
            rate = 0.25;
        }
    }

    return rate * length_of_call;
}

int main(int argc, char const *argv[])
{

    cout << "- Su @12:30 (45m) = $";
    cout << compute_cost("Su", "12:30", 45) << endl; // 0.15
    
    cout << "- Mo @12:30 (45m) = $";
    cout << compute_cost("Mo", "12:30", 45) << endl; // 0.40
    
    cout << "- Mo @19:30 (45m) = $";
    cout << compute_cost("Mo", "19:30", 45) << endl; // 0.25

    return 0;
}
