#include <iostream>

using namespace std;

bool is_leapyear(int year) {
    if(year % 400 == 0) {
        return true;
    }
    if(year % 4 == 0 and year % 100 > 0) {
        return true;
    }
    return false;
}

void getDayOfWeek(int month, int day, int year) {
    int century = (year - (year % 100)) / 100;
    int century_value = (3 - (century % 4)) * 2;

    int year_value = (year % 100) + ((year % 100) / 4);

    int month_value;

    switch (month) {
    case 1:
        month_value = is_leapyear(year) ? 6 : 0;
        break;
    case 2:
        month_value = is_leapyear(year) ? 2 : 3;
        break;
    case 3:
        month_value = 3;
        break;
    case 4:
        month_value = 6;
        break;
    case 5:
        month_value = 1;
        break;
    case 6:
        month_value = 4;
        break;
    case 7:
        month_value = 6;
        break;
    case 8:
        month_value = 2;
        break;
    case 9:
        month_value = 5;
        break;
    case 10:
        month_value = 0;
        break;
    case 11:
        month_value = 3;
        break;
    case 12:
        month_value = 5;
        break;
    default:
        break;
    }

    int day_of_week = (century_value + year_value + month_value + day) % 7;
    
    char const *weekdays[7] = {
        "sunday",
        "monday",
        "tuesday",
        "wednesday",
        "thursday",
        "friday",
        "saturday"
    };
    cout << "That date is/was a " << weekdays[day_of_week] << "." << endl;
}

int main(int argc, char const *argv[]) {
    getDayOfWeek(7, 4, 2008); //=> July 4, 2008; It was a Friday
    return 0;
}
