#include <iostream>
#include <map>

using namespace std;

bool test_delim(char c) {
    switch (c) {
        case ' ':
        case '.':
        case ',':
        case '\n':
        case '\r':
            return true;
            break;
        default:
            return false;
            break;
    }
}

string str_to_lower(string s) {
    string temp;
    for(auto &i: s) {
        temp += tolower(i);
    }
    return temp;
}

void countLetters(string input) {
    /* Remove trailing delimiters */
    while(test_delim(input.back())) {
        input.erase(--input.end());
    }

    /* lowercase */
    input = str_to_lower(input);
    
    /* count letters & words */
    int words = 1; // it's gotta be at least 1 word, so we start at one
    
    map<char, int> letters; // Using a map means that letters are automatically sorted!

    for(int i = 0; i < input.length(); i++) {
        if(test_delim(input.at(i))) {
            // delim means new word
            words++;
        }

        while(test_delim(input.at(i))) {
            // skip adjacent delims
            i++;
        }

        // increment the letter count
        letters[input.at(i)] ? letters[input.at(i)]++ : letters[input.at(i)] = 1;
    }

    /* pretty-print the results */
    cout << words << " words" << endl;
    for (auto &p: letters) {
        cout << p.second << " " << p.first << endl;
    }
}

int main(int argc, char const *argv[])
{
    countLetters("I say Hi."); // => 3 words, 1a 1h 2i 1s 1y
    countLetters("AaBb Cc."); // => 2 words, 2a 2b 2c
    countLetters("Dd\nEe, Ff."); // => 3 words, 2d 2e 2f
    return 0;
}
