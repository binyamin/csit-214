#include <iostream>

/*
    ---Project 1---
    Author: Binyamin Green
    Class:  CSIT 214
    Date:   9/5/2021
*/

using namespace std;

int main() {
    cout << "***********************************\n";
    cout << "\n";
    cout << "      C C C         s S S s      !!\n";
    cout << "    C       C      S       S     !!\n";
    cout << "  C               s              !!\n";
    cout << " C                 S             !!\n";
    cout << " C                  s S S s      !!\n";
    cout << " C                         S     !!\n";
    cout << "  C                         s    !!\n";
    cout << "    C       C      S       S\n";
    cout << "      C C C         s S S s      00\n";
    cout << "\n";
    cout << "***********************************\n\n";
    cout << "If you're on a Unix-like OS, run `figlet CS!` for better results :)\n";
    return 0;
}